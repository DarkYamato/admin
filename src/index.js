import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import Users from './components/Users';
import UserInfo from './components/UserInfo';
import Auth from './components/Auth';
import 'semantic-ui-css/semantic.min.css';
import './index.css';

const Root = () => (
    <Router>
        <Switch>
            <Route path="/auth" exact component={Auth} />
            <Route path="/users" component={Users} />
            <Route path="/user/:id" component={UserInfo} />
            <Redirect to="/auth" />
        </Switch>
    </Router>
)

ReactDOM.render(<Root />, document.getElementById('root'));
