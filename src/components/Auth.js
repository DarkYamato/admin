import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { GoogleLogin } from "react-google-login";

import "../styles/Auth.css"


class Auth extends Component {
    state = {
        login: false
    }

    componentDidMount() {
        localStorage.getItem('login') === 'success' && this.setState({ login: true });
    }
    responseGoogle = res => {
        localStorage.setItem('login', 'success')
        this.setState({ login: true })
    };

    render() {
        if (this.state.login) {
            return <Redirect push to='/users' />
        }
        return (
            <div className="login">
                <div>
                    <div className="auth-title">Authorization</div>
                    <GoogleLogin
                        clientId="826842466367-4bqs5snia820im1mrktlu476jrb612ho.apps.googleusercontent.com"
                        buttonText="Sign in with Google"
                        onSuccess={this.responseGoogle}
                        onFailure={this.responseGoogleError}
                        className="ui button button-auth"
                    />
                </div>
            </div>
        )
    }
}

export default Auth;