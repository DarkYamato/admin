import React, { Component } from 'react';
import { Table } from 'semantic-ui-react'
import { Link, Redirect } from "react-router-dom";
import Pagination from "react-js-pagination";

import '../styles/Users.css'

class Users extends Component {
    state = {
        users: [],
        totalSize: 100,
        activePage: 1,
        offset: 10,
        login: true
    }

    componentDidMount() {
        if (localStorage.getItem('data')) {
            const localData = JSON.parse(localStorage.getItem('data'));
            const localUsers = Object.keys(localData).map(i => localData[i]);

            this.setState({ users: localUsers })
        } else {
            this.getUsers();
        }
        localStorage.getItem('login') === 'success' ? this.setState({ login: true }) : this.setState({ login: false });
    }

    getUsers = (offset = 0, activePage = 1) => {
        const token = '18476dc1c4fb11f4eebd2c4aaacdb3c14b3cd1e945dd8bc8456b73c8d4ef33cf';

        fetch(`https://front-test.now.sh/users?token=${token}&offset=${offset}`)
            .then(res => res.json())
            .then(res => {
                this.setState({ users: res.data, totalSize: res.pagination.count });
                return res.data;
            })
            .then(res => localStorage.setItem('data', JSON.stringify(res)))
    }

    handlePageChange = (page) => {
        const offset = page === 1 ? 0 : (page - 1) * 10;
        this.setState({ activePage: page });
        this.getUsers(offset)
    }

    handleLogout = () => {
        localStorage.setItem('login', '');
        localStorage.setItem('data', '');

        this.setState({ login: '' })
    }

    render() {
        const { offset, activePage, totalSize } = this.state;

        if (!this.state.login) {
            return <Redirect push to='/auth' />
        }
        return (
            <div className="users">
                <div className="header-container ">
                    <div className="title">Users</div>
                    <div className="header-logout" onClick={this.handleLogout}>Log out</div>
                </div>
                <Table celled>
                    <Table.Header>
                        <tr >
                            <th >ID</th>
                            <th >Full Name</th>
                            <th >E-mail</th>
                        </tr>
                    </Table.Header>
                    <Table.Body>
                        {this.state.users && this.state.users.map((x, i) =>
                            <Table.Row className="table-line" key={i}>
                                <Td to={`/user/${i + 1}`}>{x.id}</Td>
                                <Td to={`/user/${i + 1}`}>{x.fullName}</Td>
                                <Td to={`/user/${i + 1}`}>{x.email}</Td>
                            </Table.Row>
                        )}
                    </Table.Body>
                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan='3'>
                                <Pagination activePage={activePage}
                                    hideFirstLastPages
                                    hideNavigation
                                    totalItemsCount={totalSize}
                                    onChange={this.handlePageChange}
                                    itemsCountPerPage={offset}
                                    pageRangeDisplayed={offset} />
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
            </div>
        );
    }
}

function Td({ children, to }) {
    const content = to ? (
        <Link className="content-line" to={to}>{children}</Link>
    ) : (
            <div className="content-line">{children}</div>
        );

    return (
        <td>
            {content}
        </td>
    );
}

export default Users;