import React, { Component } from 'react';
import { Container, Divider, Button, Input, Form, TextArea } from 'semantic-ui-react';
import { Map, Marker } from 'yandex-map-react';
import { Link, Redirect } from "react-router-dom";

import '../styles/UserInfo.css'

class UserInfo extends Component {
    state = {
        user: {},
        image: '',
        about: '',
        disabled: true,
        login: true
    };

    componentDidMount() {
        this.getUser(this.props.match.params.id);
        localStorage.getItem('login') === 'success' ? this.setState({ login: true }) : this.setState({ login: false });
    };

    getUser = (id) => {
        const user = JSON.parse(localStorage.getItem('data'));

        this.setState({ user: user[id - 1] || {} });
        this.setState({ about: user[id - 1] ? user[id - 1].text : '' });
    };

    setUserImg = (id, img) => {
        const user = Object.assign({}, JSON.parse(localStorage.getItem('data')));

        user[id - 1].image = img;
        localStorage.setItem('data', JSON.stringify(user));
    }

    setUserDescription = (id, text) => {
        const user = Object.assign({}, JSON.parse(localStorage.getItem('data')));

        user[id - 1].text = text;
        localStorage.setItem('data', JSON.stringify(user));
    }

    handleChange = name => e => {
        this.setState({
            [name]: e.target.value
        });
    };

    handleSubmit = () => {
        this.setUserImg(this.props.match.params.id, this.state.image);
        this.setUserDescription(this.props.match.params.id, this.state.about);
        this.getUser(this.props.match.params.id);
    }

    handleChangeImg = (e) => {
        const selectedFile = e.target.files[0];
        const reader = new FileReader();

        reader.onload = event => {
            this.setState({ image: event.target.result })
        };
        reader.readAsDataURL(selectedFile);
    }

    getInfo = () => {
        JSON.parse(localStorage.getItem('data'));
    };

    toggleEdit = () => {
        this.setState({
            disabled: !this.state.disabled
        });
    }

    handleLogout = () => {
        localStorage.setItem('login', '');
        localStorage.setItem('data', '');
        this.setState({ login: '' })
    }

    render() {
        const { user, about } = this.state;

        if (!this.state.login) {
            return <Redirect push to='/auth' />
        }
        return (
            <div className="user-info">
                <div className="header-container ">
                    <Link to={'/users/'}><div className="header-title">Users</div></Link>
                    <div className="header-logout" onClick={this.handleLogout}>Log Out</div>
                </div>
                <Container textAlign='justified' className='user-container'>
                    <b className='user-title'>User Info</b>
                    <Divider />
                    <div className="user-info-content">
                        <div className='content'>
                            <div className='content-info'>
                                <span className='header'>ID:</span> <span className='info-item'>{user.id}</span>
                            </div>
                            <div className='content-info'>
                                <span className='header'>First Name:</span> <span className='info-item'>{user.firstName}</span>
                            </div>
                            <div className='content-info'>
                                <span className='header'>Last Name:</span> <span className='info-item'>{user.lastName}</span>
                            </div>
                            <div className='content-info'>
                                <span className='header'>E-mail:</span> <span className='info-item'>{user.email}</span>
                            </div>
                            <div className="upload-image">
                                <Input placeholder='Paste user image URL...' onChange={this.handleChange('image')} value={this.state.image} type="text" />
                                <div className="upload-divider">or</div>
                                <Input type="file" onChange={this.handleChangeImg} />
                            </div>
                            <div className="content-description">
                                <div className="header">Description</div>
                                <Button onClick={this.toggleEdit} className="edit-button">Edit</Button>
                            </div>
                            <Form>
                                <TextArea autoHeight value={about} eplaceholder='Nothing here...' onChange={this.handleChange('about')} rows="10" disabled={this.state.disabled}></TextArea>
                            </Form>
                        </div>
                        <div className="arbitrary-info">
                            <div className="info-image">
                                <img className="image" src={user.image || this.state.image} alt="" />
                            </div>
                            {user.location && <Map width={270} height={270} center={[+user.location[0], +user.location[1]]} zoom={10}>
                                <Marker lat={+user.location[0]} lon={+user.location[1]} />
                            </Map>}
                        </div>
                    </div>
                    <div className="save-button">
                        <Button primary onClick={this.handleSubmit}>Save Changes</Button>
                    </div>
                </Container>
            </div>
        );
    }
}

export default UserInfo;
